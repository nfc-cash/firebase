// Initialize Firebase
var config = {
    apiKey: "AIzaSyDgBMEShHUhHfNAyDuqNLk-TTQYqCGiLyQ",
    authDomain: "nfc-cash.firebaseapp.com",
    databaseURL: "https://nfc-cash.firebaseio.com",
    projectId: "nfc-cash",
    storageBucket: "nfc-cash.appspot.com",
    messagingSenderId: "512694374172"
};
firebase.initializeApp(config);

const db = firebase.firestore();

firebase.auth().onAuthStateChanged(function(user) {
    if (!(user)) {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithRedirect(provider).then(function(result) {
            var token = result.credential.accessToken;
            var user = result.user;
        }).catch(function(error) {
            console.error("Error code: " + error.code + "\nError message: " + error.message + "\nError email: " + error.email);
        });
    } else {
        db.collection("users").doc(user.uid).onSnapshot(function(doc) {
            if (doc.exists) {
                var rank = rankToStr(doc.data().rank)
                if (rank != page) {
                    window.location.href = rank + ".html"
                }
            }
        })
    }
})


function rankToStr(rank) {
    switch (rank) {
        case 0:
            return ("disabled")
            break;
        case 1:
            return ("setup")
            break;
        case 2:
            return ("overview")
            break;
        case 3:
            return ("admin")
            break;
    }
}
