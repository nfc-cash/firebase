firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        //Script here
        let credit = 0
        db.collection("history").where("user", "==", db.collection('users').doc(user.uid)).orderBy("timestamp").onSnapshot(function(snapshot) {
            snapshot.docChanges().forEach(function(change) {
                credit = credit + change.doc.data().amount
                if(change.type == 'added') {
                    renderHistory(change.doc.data())
                }
            });
            document.querySelector('#credit').innerHTML = credit.toFixed(2) + " EUR";
            if (credit == 0) {
                document.querySelector('#topup').innerHTML = "Guthaben bitte an der Cafeteria aufladen";
            }
        });
    }
});


function renderHistory(doc) {
    var table = document.getElementById("history");

    let tr = document.createElement('tr');
    
    let amount = document.createElement('td');
    let time = document.createElement('td');

    var m = doc.timestamp.toDate();
    var timestamp =
        m.getUTCFullYear() + "/" +
        ("0" + (m.getUTCMonth()+1)).slice(-2) + "/" +
        ("0" + m.getUTCDate()).slice(-2) + " " +
        ("0" + m.getUTCHours()).slice(-2) + ":" +
        ("0" + m.getUTCMinutes()).slice(-2);

    amount.textContent = doc.amount.toFixed(2) + ' EUR';
    time.textContent = timestamp

    tr.appendChild(amount);
    tr.appendChild(time);

    $('#history tr:first').after(tr);
}