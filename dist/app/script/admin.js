firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
        
    }
});


var modal = document.getElementById('processBox');
var cancelBtn = document.getElementById("btnAbort");
var amountInfo = document.querySelector('#amount')
var statusInfo = document.querySelector('#status')
var preAmount = document.getElementById('preAmount')

function setAmount(amount) {
    preAmount.value = amount.toFixed(2);
}

setAmount(0)

function processTransaction() {
    cancelBtn.disabled = false;
    amountInfo.innerHTML = "";
    statusInfo.innerHTML = "";
    amountInfo.innerHTML = parseFloat(preAmount.value).toFixed(2);
    statusInfo.innerHTML = "Anfrage wird gestellt..";

    modal.style.display = "block";


    db.collection("bridge").doc("process").set({
        status: "requestPurchase",
        amount: parseFloat(preAmount.value),
    })
    .catch(function(error) {
        console.error("Error writing document: ", error);
    });
    



    db.collection("bridge")
    .onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change) {
            if(change.type == 'added' || change.type == 'modified') {
                switch (change.doc.data().status) {
                    case "requestPurchase":
                        document.querySelector('#status').innerHTML = "Anfrage gestellt...";
                        break;
                    case "success":
                        document.querySelector('#status').innerHTML = "Zahlung erfolgreich!";
                        cancelBtn.disabled = true;
                        setTimeout(function(){
                            setAmount(0)
                            modal.style.display = "none";
                        }, 2000);
                        break;
                    case "error":
                        document.querySelector('#status').innerHTML = "Karte nicht lesbar!";
                        cancelBtn.disabled = false;
                        break;
                    case "insufficientCredit":
                        document.querySelector('#status').innerHTML = "Guthaben nicht ausreichend!";
                        cancelBtn.disabled = false;
                        break;
                }
            }
        });
    });
}

function processTopup() {
    cancelBtn.disabled = false;
    amountInfo.innerHTML = "";
    statusInfo.innerHTML = "";
    amountInfo.innerHTML = parseFloat(preAmount.value).toFixed(2);
    statusInfo.innerHTML = "Anfrage wird gestellt..";

    modal.style.display = "block";

    db.collection("bridge").doc("process").set({
        status: "requestTopup",
        amount: parseFloat(preAmount.value),
    })
    .catch(function(error) {
        console.error("Error writing document: ", error);
    });



    db.collection("bridge")
    .onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change) {
            if(change.type == 'added' || change.type == 'modified') {
                switch (change.doc.data().status) {
                    case "requestTopup":
                        document.querySelector('#status').innerHTML = "Anfrage gestellt...";
                        break;
                    case "success":
                        document.querySelector('#status').innerHTML = "Zahlung erfolgreich!";
                        setTimeout(function(){
                            modal.style.display = "none";
                        }, 2000);
                        break;
                }
            }
        });
    });
}

function abort() {
    cancelBtn.disabled = true;
    statusInfo.innerHTML = "Anfrage gestellt...";

    var docRef = db.collection("bridge").doc("process");

    setAmount(0)

    return docRef.update({
        status: "requestAbortion"
    })
    .then(function() {
        console.log("Aborted!");
    })
    .catch(function(error) {
        // The document probably doesn't exist.
        console.log("Nothing to abort")
        console.error("Error updating document: ", error);
    });
}
