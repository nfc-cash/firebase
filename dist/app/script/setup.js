function validate() {
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            uid = document.getElementById("uid").value;
            if (uid == "") {
                document.getElementById("alert").innerHTML = "Eingabe erforderlich"
            } else {
                var docRef = db.collection("nfcChips").doc(uid);
                docRef.get().then(function(doc) {
                    if (doc.exists) {
                        if (typeof doc.data().holder == 'undefined') {
                            docRef.set({
                                holder: db.collection('users').doc(user.uid)
                            })
                            .then(function() {
                                console.log("Document successfully written!");
                                db.collection("users").doc(user.uid).update({
                                    rank: 2,
                                })
                                .then(function() {
                                    console.log("Document successfully written!");
                                })
                                .catch(function(error) {
                                    console.error("Error writing document: ", error);
                                });
                            })
                            .catch(function(error) {
                                console.error("Error writing document: ", error);
                            });
                        } else {
                            document.getElementById("alert").innerHTML = "Diese Karte ist bereits registriert!"
                        }
                    } else {
                        document.getElementById("alert").innerHTML = "Karte unbekannt"
                    }
                }).catch(function(error) {
                    console.error("Error getting document:", error);
                    document.getElementById("alert").innerHTML = "Fehler!"
                });
            }
        }
    });
}