//Require Functions
const functions = require('firebase-functions');

//Require Firebase Admin
const admin = require('firebase-admin');

//Require Actions on Google for Dialogflow Fulfillments
const {
    dialogflow
} = require('actions-on-google');

//Initialize Firebase
admin.initializeApp(functions.config().firebase);
const db = admin.firestore();

//Initialize Dialogflow
const app = dialogflow({
    debug: true
});


//Firebase functions begin

// Add data to database on user creation
exports.addUserDataOnCreation = functions.auth.user().onCreate((user) => {
    //Perform query
    db.collection("users").doc(user.uid).set({
        displayName: user.displayName,
        email: user.email,
        photoURL: user.photoURL,
        rank: 1
    }).then(function() {
        console.log("User " + user.email + " created!");
    }).catch(function(error) {
        console.error("Error writing document: ", error);
    });
    return 0;
});


// Transaction processer
exports.processTransaction = functions.https.onRequest(async(req, res) => {
    try {
        const uid = req.query.uid;

        var data = await getProcessData();
        const status = data['status'];
        const amount = data['amount'];
        const holder = await getHolder(uid);

        if (status == "requestPurchase") {
            const credit = await getCredit(holder);
            if (credit >= amount) {
                const negAmount = amount * -1;
                completeTransaction(holder, negAmount)
                setStatus('success')
                res.send("success")
            } else {
                setStatus('insufficientCredit')
                throw("insufficientCredit")
            }
        } else if (status == "requestTopup") {
            completeTransaction(holder, amount)
            setStatus('success')
            res.send("success")
        }
    } catch(err) {
        if (err == 'insufficientCredit') {
            setStatus(err)
            res.send(err)
        } else {
            setStatus('error')
            res.send("error")
        }    
    }
});

exports.requestCredit = functions.https.onRequest(async(req, res) => {
    try {
        const uid = req.query.uid;
        const holder = await getHolder(uid);
        const credit = await getCredit(holder)
        res.send(String(credit))
    } catch(err) {
        console.error(err)
        res.send(err)
    }
});


async function getProcessData() {
    var doc = await db.collection("bridge").doc('process').get()
    return doc.data()
}

async function getHolder(uid) {
    doc = await db.collection("nfcChips").doc(String(uid)).get()
    if (doc.data().holder) {
        return doc.data().holder
    } else {
        throw("unknownCard")
    }
}

async function getCredit(user) {
    var credit = 0
    snapshot = await db.collection('history').where("user", "==", user).get();
    snapshot.forEach(doc => {
        credit += doc.data().amount
    });
    return credit
}

async function completeTransaction(user, amount) {
    db.collection('history').doc().set({
        amount: amount,
        timestamp: admin.firestore.FieldValue.serverTimestamp(),
        user: user
    });
}

async function setStatus(status) {
    await db.collection('bridge').doc('process').set({'status': status});
}



//Get credit intent
app.intent('Default', async (conv) => {
    var creditFriendly = ((c) => {
        c = c.toFixed(2).split('.')
        let s = `${c[0]} Euro`
        if (parseInt(c[1])) { s += ` und ${c[1]} Cent` }
        return s
    })(await getCredit(db.collection("users").doc("h4IHVzAvbeNGa4LSbN8EhMfvU332")))
    conv.close(`Du hast ein Guthaben von ${creditFriendly}.`)
});


//Export fulfillments
exports.fulfillment = functions.https.onRequest(app);